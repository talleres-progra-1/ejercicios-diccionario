import os

def imprimenu(proteina):
    print('Añadir un elemento al directorio    =[A]\n'
          'Editar elementos del directorio     =[B]\n'
          'Consultar un elemento del conjunto  =[C]\n'
          'Eliminar un elemento del directorio =[D]\n'
          'Imprimir todos los elementos        =[E]\n'
          'Terminar con el programa            =[F]\n\n')
    var = str(input('¿Que desea realizar?\n'))
    var2 = var.upper()
    return var2

def añadelem(proteina):
    os.system ("clear")
    protein = str(input('¿Código de la proteína?\n'))
    if protein in proteina:
        print('Esta proteína ya existe en el repositorio\n')
    else:
        descrip = str(input('¿Qué descripción le adjuntará?\n'))
        proteina.update({protein: descrip})

def editelem(proteina):
    os.system ("clear")
    for key,val in dict.items(proteina):
        print('Proteína ',key,': ',val)
    protein = str(input('\n¿Que proteína desea modificar?\n'))
    if protein in proteina:
        descrip = str(input('¿Qué descrición le adjuntará?\n'))
        proteina.get(protein)
        proteina[protein] = descrip
    else:
        print('Proteína no encontrada')

def consulem(proteina):
    os.system ("clear")
    protein = str(input('¿Qué proteína busca en el conjunto?\n'))
    if protein in proteina:
        print('La proteína ',protein,': ',proteina[protein])
    else:
        print('Proteína no encontrada')

def deletelem(proteina):
    os.system ("clear")
    protein = str(input('¿Qué proteína desea eliminar de la lista?\n'))
    if protein in proteina:
        proteina.get(protein)
        del proteina[protein]
    else:
        print('Proteína no reconocida en el repositorio\n')        

def imprimeelem(proteina):
    os.system ("clear")
    for key,val in dict.items(proteina):
        print('Proteína ',key,': ',val)

proteina = {'7BQL': 'The crystal structure of PdxI complex with the Alder-ene adduct',
            '7BQJ': 'The structure of PdxI',
            '7BVZ': 'Crystal structure of MreB5 of Spiroplasma citri boundto ADP',
            '6WTT': 'Crystals Structure of the SARS-CoV-2 (COVID-19) main protease with inhibitor GC-376',
            '7JRN': 'Crystal structure of the wild type SARS-CoV-2 papain-like protease (PLPro) with inhibitor GRL0617',
            '6TZ9': 'CryoEM reconstruction of membrane-bound ESCRT-III filament composed of CHMP1B only',
            '1MBN': 'The stereochemistry of the protein myoglobin'}
var = imprimenu(proteina)
while "F" != var:

    if var == 'A':
        añadelem(proteina)
    if var == 'B':
        editelem(proteina)
    if var == 'C':
        consulem(proteina)
    if var == 'D':
        deletelem(proteina)
    if var == 'E':
        imprimeelem(proteina)
    if (var == 'A' or var == 'B' or
        var == 'C' or var == 'D' or
        var == 'E' or var == 'F'):
        print('\n')
        var = imprimenu(proteina)
    else:
        print('Valor inválido')
        print('\n')
        var = imprimenu(proteina)