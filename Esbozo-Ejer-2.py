import os

def imprimenu(traductor):
    print('Añadir la traducción de una palabra    =[A]\n'
          'Editar la traducción de una palabra    =[B]\n'
          'Consultar la traduccion de una palabra =[C]\n'
          'Eliminar palabras del traductor        =[D]\n'
          'Imprimir todas las traducciones        =[E]\n'
          'Traducir una frase español-ingles      =[F]\n'
          'Cerrar el traducctor                   =[G]\n\n')
    var = str(input('¿Que proceso desea realizar?\n'))
    var2 = var.upper()
    return var2

def añadelem(traductor):
    os.system('clear')
    palabra = str(input('Palabra;Traducción\n')).lower()
    palabra = palabra.split(sep=';')
    if palabra[0] in traductor:
        print('Esta palabra ya se encuentra en la base de datos\n')
    else:
        traductor.update({palabra[0]: palabra[1]})

def editelem(traductor):
    os.system('clear')
    for key,val in dict.items(traductor):
        print(key,val)
    palabra = str(input('¿De que palabra desea modificar la traducción?\n')).lower()
    if palabra in traductor:
        traduccion = str(input('¿Qué traducción le adjuntará?\n')).lower()
        traductor.get(palabra)
        traductor[palabra] = traduccion
    else:
        print('Palabra no encontrada')

def consulem(traductor):
    os.system('clear')
    palabra = str(input('Palabra consultada: ')).lower()
    if palabra in traductor:
        print(palabra,traductor[palabra])
    else:
        print('Palabra no encontrada')

def deletelem(traductor):
    os.system('clear')
    palabra = str(input('¿Qué palabra desea eliminar del conjunto?\n')).lower()
    if palabra in traductor:
        traductor.get(palabra)
        del traductor[palabra]
    else:
        print('Palabra no reconocida por el traductor\n')

def imprimeelem(traductor):
    os.system('clear')
    for key,val in dict.items(traductor):
        print(key,val)

def traducfrase(traductor):
    os.system('clear')
    frase = str(input('Ingrese que frase va a traducir: '))
    frase = frase.lower().split()
    for i in frase:
        print(traductor[i],end=" ")

traductor = {}
var = imprimenu(traductor)
while "G" != var:

    if var == 'A':
        añadelem(traductor)
    if var == 'B':
        editelem(traductor)
    if var == 'C':
        consulem(traductor)
    if var == 'D':
        deletelem(traductor)
    if var == 'E':
        imprimeelem(traductor)
    if var == 'F':
        traducfrase(traductor)
    if (var == 'A' or var == 'B' or
        var == 'C' or var == 'D' or
        var == 'E' or var == 'F' or
        var == 'G'):
        print('\n')
        var = imprimenu(traductor)
    else:
        print('Valor inválido')
        print('\n')
        var = imprimenu(traductor)